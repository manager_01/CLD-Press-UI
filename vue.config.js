/**
 * 配置参考: https://cli.vuejs.org/zh/config/
 */
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  chainWebpack: config => {
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .test(/\.svg$/)
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
  },
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8001,
    overlay: {
      errors: true,
      warnings: true,
    },
    proxy: {
      "/": {
        // 这个意思是：原先以 /api 开头的请求
        target: "http://localhost:9000/renren-admin", // 凡是以 /api 开头的请求，通通请求这个服务器
        changeOrigin: true, // 允许跨域
        pathRewrite: {
          "^/": "/",
          //pathRewrite: {'^/api': '/'} 重写之后url为 http://localhost:9000/xxxx
          //pathRewrite: {'^/api': '/api'} 重写之后url为http://localhost:8080/api/xxxx
        },
      },
    },
  }
}
